import View from './view';
import FighterView from './fighterView';

class FightersView extends View {
  constructor(fighters, handleClick) {
    super();

    this.createFighters(fighters, handleClick);
  }

  createFighters(fighters, handleClick) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }
}

export default FightersView;