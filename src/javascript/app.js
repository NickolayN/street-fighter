import FightersView from './fightersView';
import {
  fighterService
} from './services/fightersService';
import GameMenuView from './gameMenuView';
import Modal from './modalView';
import Fight from './fight';

class App {
  constructor() {
    this.startApp();

    this.handleFighterClick = this.handleFighterClick.bind(this);
    this.handleSubmit = this.handleDetailsSubmit.bind(this);
    this.handleSelect = this.handleFighterSelect.bind(this);
    this.handleFightClick = this.handleFightClick.bind(this);
  }

  fightersDetailsMap = new Map();
  selectedFighters = [null, null];

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      this.fightersView = new FightersView(fighters, this.handleFighterClick);
      const fightersElement = this.fightersView.element;
      
      const gameMenu = this.createGameMenu(this.selectedFighters, this.handleFightClick);
      App.rootElement.append(gameMenu);

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

  createGameMenu(fighters, handle) {
    this.gameMenu = new GameMenuView(fighters, handle);
    return this.gameMenu.element;
  }

  updateGameMenu(fighters, handle) {
    const gameMenu = new GameMenuView(fighters, handle);
    this.gameMenu.element.replaceWith(gameMenu.element);
    this.gameMenu = gameMenu;
  }

  async handleFighterClick(event, fighter) {
    if (!this.fightersDetailsMap.has(fighter._id)) {
      this.showLoader();
      try {
        const details = await fighterService.getFighterDetails(fighter._id);
        this.fightersDetailsMap.set(fighter._id, details);
      } catch (error) {
        console.warn(error);
      }
    }
    
    this.loader.remove();

    this.showDetails(      
      this.fightersDetailsMap.get(fighter._id),
      this.handleSubmit,
      this.handleSelect
    );
  }

  showLoader() {
    this.loader = document.createElement('div');
    this.loader.className = 'loader';
    document.body.appendChild(this.loader);
  }

  handleFighterSelect(event, id) {
    event.preventDefault();

    const fighter = this.fightersDetailsMap.get(id);
    this.selectFighter(fighter);
    this.modal.element.remove();

    this.updateGameMenu(this.selectedFighters, this.handleFightClick);
  }

  selectFighter(fighter) {
    if (!this.selectedFighters[0]) {
      this.selectedFighters[0] = fighter;
      return;
    }

    this.selectedFighters[1] = fighter;
  }

  handleDetailsSubmit(fighter) {
    this.fightersDetailsMap.delete(fighter._id);
    this.fightersDetailsMap.set(fighter._id, fighter);
    this.modal.element.remove();
  }

  handleFightClick(event) {
    this.startFight();
  }

  startFight() {
    this.fight = new Fight(...this.selectedFighters);
    this.fight.onEnd = this.endFight.bind(this);

    this.fightView = this.fight.element;
    this.fightersView.element.replaceWith(this.fightView);
    this.fight.start();
  }

  endFight() {
    this.fightView.replaceWith(this.fightersView.element);
  }

  showDetails(fighter, handleSubmit, handleSelect) {
    const modal = new Modal(fighter, handleSubmit, handleSelect);
    document.body.append(modal.element);
    this.modal = modal;
  }
}

export default App;