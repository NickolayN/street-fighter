import { callApi } from '../helpers/apiHelper';

class FighterService {
  getFighters() {
    const endpoint = 'fighters.json';

    return this._getData(endpoint);
  }

  getFighterDetails(_id) {
    const endpoint = `details/fighter/${_id}.json`;

    return this._getData(endpoint);
  }

  async _getData(endpoint) {
    try {      
      const apiResult = await callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
