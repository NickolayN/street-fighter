import View from './view';
import FighterView from './fighterView';

class FightView extends View {
  constructor(firstFighter, secondFighter) {
    super();

    this.firstFighter = {...firstFighter};
    this.secondFighter = {...secondFighter};

    this.createFight();
  }

  createFight() {
    this.element = this.createElement({
      tagName: 'div',
      className: 'fight'
    });

    const firstFighter = this.createFighter(this.firstFighter);
    const secondFighter = this.createFighter(this.secondFighter);

    this.element.append(firstFighter, secondFighter);
  }

  createFighter(fighter) {
    const field = this.createElement({tagName: 'div', className: 'battle-field'});
    const fighterElement = new FighterView(fighter).element;

    return fighterElement;
  }
}

export default FightView;