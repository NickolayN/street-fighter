import View from './view';

class GameMenuView extends View {
  constructor(fighters, handleClick) {
    super();

    this.createGameMenu(fighters, handleClick);
  }

  createGameMenu(fighters, handleClick) {
    this.element = this.createElement({tagName: 'div', className: 'game-menu'});
    const fightersElements = fighters.map(fighter => this.createFighter(fighter))
    const startGameButton = this.createStartButton();
    startGameButton.addEventListener('click', event => handleClick(event));

    this.element.append(fightersElements[0], startGameButton, fightersElements[1]);
  }

  createFighter(fighter) {
    const fighterElem = this.createElement({tagName: 'span', className: 'fighter-option'});    

    if (!fighter) {
      fighterElem.innerText = 'Choose fighter';
    } else {
      fighterElem.classList.add('selected');
      fighterElem.innerText = fighter.name;
    }  

    return fighterElem;
  }

  createStartButton() {
    const startButton = this.createElement({
      tagName: 'button',
      className: 'button start-game-button'
    });
    startButton.innerText = 'Fight!';    

    return startButton;
  }
}

export default GameMenuView;