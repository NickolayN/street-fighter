import Fighter from './fighter';
import FightView from './fightView';

const ATTACK_TIME = 10;

class Fight {
  constructor(firstFighter, secondFighter) {
    this.fighters = [
      new Fighter(firstFighter),
      new Fighter(secondFighter)
    ];

    this.state = {
      firstFighterIsNext: true,
      firstFighterHealth: this.fighters[0].details.health,
      secondFighterHealth: this.fighters[1].details.health
    }
  }

  round() {
    let health = this.state.firstFighterIsNext ?
      this.state.secondFighterHealth :
      this.state.firstFighterHealth;

    const [attacker, defender] = this.state.firstFighterIsNext ?
      this.fighters :
      [...this.fighters].reverse();

    health = health - this.calculateDamage(attacker, defender);

    if (this.state.firstFighterIsNext) {
      this.state = {
        ...this.state,
        secondFighterHealth: health,
        firstFighterIsNext: !this.state.firstFighterIsNext
      };
    } else {
      this.state = {
        ...this.state,
        firstFighterHealth: health,
        firstFighterIsNext: !this.state.firstFighterIsNext
      }
    }

    if (health <= 0) {
      return attacker;
    }
  }

  start() {
    let winner = this.round();

    if (!winner) {
      setTimeout(() => {
        this.start();
      }, ATTACK_TIME);
    } else {
      this.endGame(winner);
    }
  }

  onEnd() {}

  endGame(winner) {
    alert(`Winner: ${winner.details.name}`);
    this.onEnd();
  }

  calculateDamage(attacker, defender) {
    return attacker.getHitPower() - defender.getBlockPower();
  }

  calculateWinner(firstFighterHealth, secondFighterHealth) {
    return !firstFighterHealth 
  }

  get element() {
    this.view = new FightView(this.fighters[0].details, this.fighters[1].details);
    return this.view.element;
  }
}

export default Fight;