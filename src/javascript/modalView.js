import View from './view';

class Modal extends View {
  constructor(fighter, onSubmit, onSelect) {
    super();

    this.fighter = {...fighter};
    this.createModal(fighter, onSubmit, onSelect);
  }

  createModal(fighter, onSubmit, onSelect) {
    const {health, attack, defense} = fighter;
    this.element = this.createElement({
      tagName: 'form',
      className: 'modal'
    });

    const defenseElem = this.createItem('defense', defense);
    const healthElem = this.createItem('health', health);
    const attackElem = this.createItem('attack', attack);
    const submit = this.createSubmitButton();
    const selectButton = this.createSelectButton();
    this.element.append(healthElem, attackElem, defenseElem, submit, selectButton);

    this.element.addEventListener('submit', event => this.onSubmit(event, onSubmit));
    selectButton.addEventListener('click', event => onSelect(event, fighter._id));
  }

  createItem(name, value) {
    const element = this.createElement({
      tagName: 'div',
      className: 'modal-item'
    });

    const label = this.createLabel(name);
    const input = this.createInput(name, value);
    

    element.append(label, input);

    return element;
  }

  createLabel(inputId) {
    const attributes = {for: inputId};
    const labelElem = this.createElement({tagName: 'label', className: 'modal-label', attributes});
    labelElem.textContent = inputId;
    
    return labelElem;
  }

  createInput(name, value) {
    const MIN_VALUE = 0;

    const attributes = {
      name,
      value,
      type: 'number',
      id: name,
      min: MIN_VALUE
    }
    const element = this.createElement({
      tagName: 'input',
      className: 'modal-input',
      attributes
    });

    return element;
  }

  createSubmitButton() {
    const buttonElem = this.createElement({
      tagName: 'button',
      className: 'button modal-submit',
      attributes: {type: 'submit'}
    });

    buttonElem.textContent = 'Ok';

    return buttonElem;
  }

  createSelectButton() {
    const buttonElem = this.createElement({
      tagName: 'button',
      className: 'button modal-select'
    });

    buttonElem.textContent = 'Select';

    return buttonElem;
  }

  onSubmit(event, handler) {
    event.preventDefault();

    const form = this.element;
    const health = form.health.value;
    const attack = form.attack.value;
    const defense = form.defense.value;

    const fighter = {...this.fighter, health, attack, defense};
    handler(fighter);
  }
}

export default Modal;