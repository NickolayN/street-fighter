class Fighter {
  constructor(fighter) {    
    this.details = {...fighter};
  }

  getHitPower() {
    const criticalHitChance = this._getRandom(1, 2);
    const power = this.details.attack * criticalHitChance;
    return power;
  }

  getBlockPower() {
    const dodgeChance = this._getRandom(1, 2);
    const power = this.details.defense * dodgeChance;
    return power;
  }

  _getRandom(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
  }
}

export default Fighter;